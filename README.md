# caja-script-audacious
Script for caja for adding files to a playlist on audacious

This script works on 'caja' and also it could work on nautilus but I didn't test on it.

# Dependencies
	- zenity
	- audacious

# Installing
	1. Copy the script to folder:
		$HOME/.config/caja/scripts
	2. Restart caja:
		caja -q

# Installing on Archlinux from AUR
	You can install caja-script-audacious from AUR

# Defining a keybinding for the script on 'caja'
Run: dconf-editor

Set: org -> mate -> desktop -> interface: can-change-accels

On 'caja' go to 'file' menu and select the script, then press your new keybinding.

Unset again: org -> mate -> desktop -> interface: can-change-accels

# TODO
Support for adding folders recursively





