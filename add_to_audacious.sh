#!/bin/bash
#
# caja-script-audacious
#
# 	by 'nashgul' 	<m.alcocer1978@gmail.com>
#
# this script adds selected files to audacious playlist.
#
# Dependencies:
# 	- audacious
# 	- bash
# 	- zenity
#	- sed
#
# Installation:
#	1. Copy this script to: $HOME/.config/caja/scripts
#	2. Give it exec perm
#	3. Restart caja: 'caja -q'

configure(){
	echo "Choose your language:"
	echo "0. Spanish"
	echo "1. English"
	read choice
	(( choice != 0 && choice != 1 )) && { echo "bad choice!" ; configure ; }
	sed -i "s/^lang=\(0\|1\)/lang=$choice/" $0
	exit 0
}

add_to_next(){
	IFS=$'\n'
	local actual=$(audtool playlist-position)
	for x in $CAJA_SCRIPT_SELECTED_FILE_PATHS; do
		((actual++))
		audtool playlist-insurl "$x" $actual 
	done
}

add_to_the_end(){
	IFS=$'\n'
	for x in $CAJA_SCRIPT_SELECTED_FILE_PATHS; do
		audtool playlist-addurl "$x"
	done
}

new_list(){
	local current_playlist=$(audtool --current-playlist)
	audtool new-playlist
	add_to_the_end
	audtool set-current-playlist $current_playlist
}

exit_function(){
	exit 0
}

[[ -z $1 ]] && configure

# lang: 0=español, 1=english
lang=1

menu_entries_1=( "Añadir a audacious" "Add to audacious" )
menu_entries_2=( "Elige opción" "Choose option" )
columns_1=( "Opción" "Choice" )
columns_2=( "Acción" "Action" )
list_entries_1=( "A continuación de la canción actual" "After current song" )
list_entries_2=( "Al final de la lista actual" "At the end of current playlist" )
list_entries_3=( "A una lista nueva" "To new playlist" )
list_entries_4=( "Salir" "Exit" )

action=$(zenity --width=360 --height=320 --list --title "${menu_entries_1[$lang]}" --text "${menu_entries_2[$lang]}" --column "${columns_1[$lang]}" --column "${columns_2[$lang]}" 1 "${list_entries_1[$lang]}" 2 "${list_entries_2[$lang]}" 3 "${list_entries_3[$lang]}" 4 "${list_entries_4[$lang]}")
case $action in
	1)
		add_to_next
		;;
	2)
		add_to_the_end
		;;
	3)
		new_list
		;;
	4)
		exit_function
		;;
esac
